package model;

import entity.UrlConf;
import entity.UrlResponse;
import entity.UrlStatus;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class AnalyzerTest {
    private ResponseAnalyzer analyzer = new ResponseAnalyzer();
    private UrlPing pinger = mock(UrlPing.class);

    private UrlConf confWithSubStr = new UrlConf();
    {
        confWithSubStr.setUrl("some_url1");
        confWithSubStr.setResponseTimeOk(100);
        confWithSubStr.setResponseTimeWarning(200);
        confWithSubStr.setResponseSize(1000);
        confWithSubStr.setResponseStatus(200);
        confWithSubStr.setResponseSubstr("substr");
    }

    private UrlConf confWithoutSubStr = new UrlConf();
    {
        confWithoutSubStr.setUrl("some_url1");
        confWithoutSubStr.setResponseTimeOk(100);
        confWithoutSubStr.setResponseTimeWarning(200);
        confWithoutSubStr.setResponseSize(1000);
        confWithoutSubStr.setResponseStatus(200);
    }

    @Test
    public void testAnalyzeEverythingOkWithSubstring() throws IOException {
        when(pinger.getUrlName())
                .thenReturn("some_url1");
        when(pinger.getResponseCode())
                .thenReturn(200);
        when(pinger.getResponseTime())
                .thenReturn((long)50);
        when(pinger.getPageContentSize())
                .thenReturn(199);
        when(pinger.getPageContent())
                .thenReturn("some text. substr. some more.");
        doNothing().when(pinger).ping(any());

        analyzer.setPinger(pinger);

        UrlResponse expected = new UrlResponse();
        expected.setConf(confWithSubStr);
        expected.setStatusCode(200);
        expected.setStatusName(UrlStatus.OK);
        expected.setResponseTime(50);
        expected.setResponseSize(199);

        UrlResponse real = analyzer.analyze(confWithSubStr);

        assertEquals("testAnalyzeEverythingOkWithSubstring",
                expected, real);
    }

    @Test
    public void testAnalyzeEverythingOkWithoutSubstring() throws IOException {
        when(pinger.getUrlName())
                .thenReturn("some_url1");
        when(pinger.getResponseCode())
                .thenReturn(200);
        when(pinger.getResponseTime())
                .thenReturn((long)50);
        when(pinger.getPageContentSize())
                .thenReturn(199);
        when(pinger.getPageContent())
                .thenReturn(null);
        doNothing().when(pinger).ping(any());

        analyzer.setPinger(pinger);

        UrlResponse expected = new UrlResponse();
        expected.setConf(confWithoutSubStr);
        expected.setStatusCode(200);
        expected.setStatusName(UrlStatus.OK);
        expected.setResponseTime(50);
        expected.setResponseSize(199);

        UrlResponse real = analyzer.analyze(confWithoutSubStr);

        assertEquals("testAnalyzeEverythingOkWithoutSubstring",
                expected, real);
    }

    @Test
    public void testAnalyzeBadResponseCodeWithSubstring() throws IOException {
        when(pinger.getUrlName())
                .thenReturn("some_url1");
        when(pinger.getResponseCode())
                .thenReturn(201);
        when(pinger.getResponseTime())
                .thenReturn((long)50);
        when(pinger.getPageContentSize())
                .thenReturn(199);
        when(pinger.getPageContent())
                .thenReturn("some text. substr. some more.");
        doNothing().when(pinger).ping(any());

        analyzer.setPinger(pinger);

        UrlResponse expected = new UrlResponse();
        expected.setConf(confWithSubStr);
        expected.setStatusCode(201);
        expected.setStatusName(UrlStatus.CRITICAL);
        expected.setResponseTime(50);
        expected.setResponseSize(199);

        UrlResponse real = analyzer.analyze(confWithSubStr);

        assertEquals("testAnalyzeBadResponseCodeWithSubstring",
                expected, real);
    }

    @Test
    public void testAnalyzeBadResponseCodeWithoutSubstring() throws IOException {
        when(pinger.getUrlName())
                .thenReturn("some_url1");
        when(pinger.getResponseCode())
                .thenReturn(201);
        when(pinger.getResponseTime())
                .thenReturn((long)50);
        when(pinger.getPageContentSize())
                .thenReturn(199);
        when(pinger.getPageContent())
                .thenReturn(null);
        doNothing().when(pinger).ping(any());

        analyzer.setPinger(pinger);

        UrlResponse expected = new UrlResponse();
        expected.setConf(confWithoutSubStr);
        expected.setStatusCode(201);
        expected.setStatusName(UrlStatus.CRITICAL);
        expected.setResponseTime(50);
        expected.setResponseSize(199);

        UrlResponse real = analyzer.analyze(confWithoutSubStr);

        assertEquals("testAnalyzeBadResponseCodeWithoutSubstring",
                expected, real);
    }

    @Test
    public void testAnalyzeBadSubstring() throws IOException {
        when(pinger.getUrlName())
                .thenReturn("some_url1");
        when(pinger.getResponseCode())
                .thenReturn(200);
        when(pinger.getResponseTime())
                .thenReturn((long)50);
        when(pinger.getPageContentSize())
                .thenReturn(199);
        when(pinger.getPageContent())
                .thenReturn("some text. some more.");
        doNothing().when(pinger).ping(any());

        analyzer.setPinger(pinger);

        UrlResponse expected = new UrlResponse();
        expected.setConf(confWithSubStr);
        expected.setStatusCode(200);
        expected.setStatusName(UrlStatus.CRITICAL);
        expected.setResponseTime(50);
        expected.setResponseSize(199);

        UrlResponse real = analyzer.analyze(confWithSubStr);

        assertEquals("testAnalyzeBadSubstring",
                expected, real);
    }

    @Test
    public void testAnalyzeCriticalResponseTime() throws IOException {
        when(pinger.getUrlName())
                .thenReturn("some_url1");
        when(pinger.getResponseCode())
                .thenReturn(200);
        when(pinger.getResponseTime())
                .thenReturn((long)2000);
        when(pinger.getPageContentSize())
                .thenReturn(199);
        when(pinger.getPageContent())
                .thenReturn("some text. substr. some more.");
        doNothing().when(pinger).ping(any());

        analyzer.setPinger(pinger);

        UrlResponse expected = new UrlResponse();
        expected.setConf(confWithSubStr);
        expected.setStatusCode(200);
        expected.setStatusName(UrlStatus.CRITICAL);
        expected.setResponseTime(2000);
        expected.setResponseSize(199);

        UrlResponse real = analyzer.analyze(confWithSubStr);

        assertEquals("testAnalyzeCriticalResponseTime",
                expected, real);
    }

    @Test
    public void testAnalyzeWarningResponseTime() throws IOException {
        when(pinger.getUrlName())
                .thenReturn("some_url1");
        when(pinger.getResponseCode())
                .thenReturn(200);
        when(pinger.getResponseTime())
                .thenReturn((long)101);
        when(pinger.getPageContentSize())
                .thenReturn(199);
        when(pinger.getPageContent())
                .thenReturn("some text. substr. some more.");
        doNothing().when(pinger).ping(any());

        analyzer.setPinger(pinger);

        UrlResponse expected = new UrlResponse();
        expected.setConf(confWithSubStr);
        expected.setStatusCode(200);
        expected.setStatusName(UrlStatus.WARNING);
        expected.setResponseTime(101);
        expected.setResponseSize(199);

        UrlResponse real = analyzer.analyze(confWithSubStr);

        assertEquals("testAnalyzeWarningResponseTime",
                expected, real);
    }

    @Test
    public void testAnalyzeBadResponseSize() throws IOException {
        when(pinger.getUrlName())
                .thenReturn("some_url1");
        when(pinger.getResponseCode())
                .thenReturn(200);
        when(pinger.getResponseTime())
                .thenReturn((long)50);
        when(pinger.getPageContentSize())
                .thenReturn(2000);
        when(pinger.getPageContent())
                .thenReturn("some text. substr. some more.");
        doNothing().when(pinger).ping(any());

        analyzer.setPinger(pinger);

        UrlResponse expected = new UrlResponse();
        expected.setConf(confWithSubStr);
        expected.setStatusCode(200);
        expected.setStatusName(UrlStatus.CRITICAL);
        expected.setResponseTime(50);
        expected.setResponseSize(2000);

        UrlResponse real = analyzer.analyze(confWithSubStr);

        assertEquals("testAnalyzeBadResponseSize",
                expected, real);
    }

    @Test
    public void testAnalyzeBadResponseSizeAndWarningResponseTime() throws IOException {
        when(pinger.getUrlName())
                .thenReturn("some_url1");
        when(pinger.getResponseCode())
                .thenReturn(200);
        when(pinger.getResponseTime())
                .thenReturn((long)101);
        when(pinger.getPageContentSize())
                .thenReturn(2000);
        when(pinger.getPageContent())
                .thenReturn("some text. substr. some more.");
        doNothing().when(pinger).ping(any());

        analyzer.setPinger(pinger);

        UrlResponse expected = new UrlResponse();
        expected.setConf(confWithSubStr);
        expected.setStatusCode(200);
        expected.setStatusName(UrlStatus.CRITICAL);
        expected.setResponseTime(101);
        expected.setResponseSize(2000);

        UrlResponse real = analyzer.analyze(confWithSubStr);

        assertEquals("testAnalyzeBadResponseSizeAndWarningResponseTime",
                expected, real);
    }
}