<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <!--<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">-->
    <meta http-equiv="refresh" content="30">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <title>Welcome</title>
</head>

<body style="background-color:whitesmoke;">
    <div style="border-style:solid; background-color:#428bca; margin: auto;" id="header">
        <div>
        <h2 style="text-align: center; font-size: 3.8vw;">
            Page refreshes every 30 seconds
        </h2>
        </div>
    </div>

    <div class="container" style="margin-top: 2px;">
    <div class="row">
        <div class="col-sm-2">
        </div>

        <div class="col-sm-8">
        <div style="height:500px; overflow:auto;">
        <table class="table" >
            <tr>
                <td> Url </td>
                <td> Response Status </td>
                <td> Response Time </td>
                <td> Response Size </td>
                <td> Response Code </td>
            </tr>
            <c:forEach items="${result}" var="res">
                <tr>
                    <td>${res.conf.url}</td>
                    <td>${res.statusName}</td>
                    <td>${res.responseTime}</td>
                    <td>${res.responseSize}</td>
                    <td>${res.statusCode}</td>
                </tr>
            </c:forEach>
        </table>
        </div>
        </div>


        <div class="col-sm-2">
        <form>
            <input type="button" onClick="window.location.reload()" value="Refresh">
        </form>
        </div>
    </div>
    </div>
</body>
</html>