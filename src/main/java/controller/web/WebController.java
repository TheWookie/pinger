package controller.web;


import service.UrlPingService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/result")
public class WebController extends HttpServlet {
    private final Logger logger = LogManager.getLogger(WebController.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UrlPingService urlPingService = UrlPingService.getInstance();

        req.setAttribute("result", urlPingService.ping());

        req.getRequestDispatcher("index.jsp").forward(req, resp);
    }

}
