package controller.console;

import entity.UrlResponse;
import service.UrlPingService;
import view.View;

import java.util.Queue;

public class ConsoleController {
    private UrlPingService service;
    private View view;

    public ConsoleController(UrlPingService service, View view) {
        this.service = service;
        this.view = view;
    }

    public void process() {
        Queue<UrlResponse> result = service.ping();

        for(UrlResponse urlResponse : result) {
            view.printMessage(View.URL_NAME, urlResponse.getConf().getUrl());
            view.printMessage(View.RESPONSE_CODE, Integer.toString(urlResponse.getStatusCode()));
            view.printMessage(View.RESPONSE_MESSAGE, urlResponse.getStatusName().toString());
            view.printMessage(View.RESPONSE_TIME, Long.toString(urlResponse.getResponseTime()));
            view.printMessage(View.RESPONSE_SIZE, Integer.toString(urlResponse.getResponseSize()));
            view.printMessage(View.DIVIDER);
        }

    }
}
