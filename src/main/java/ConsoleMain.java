import controller.console.ConsoleController;
import service.UrlPingService;
import view.View;

public class ConsoleMain {
    public static void main(String[] args) {
        UrlPingService service = UrlPingService.getInstance();
        View view = new View();

        ConsoleController controller = new ConsoleController(service, view);
        controller.process();
    }
}
