package view;


public class View {
    public static final String RESPONSE_TIME = "Response time: ";
    public static final String RESPONSE_CODE = "Response code: ";
    public static final String RESPONSE_MESSAGE = "Response message: ";
    public static final String PAGE_CONTENT = "Page content: ";
    public static final String URL_NAME = "URL: ";
    public static final String RESPONSE_SIZE = "Response size: ";
    public static final String DIVIDER = "---------------------------";

    public void printMessage(String... message) {
        for(String m : message) {
            System.out.print(m);
        }

        System.out.println();
    }
}
