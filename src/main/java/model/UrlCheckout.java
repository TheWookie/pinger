package model;

import entity.UrlConf;
import entity.UrlResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Queue;

public class UrlCheckout extends Thread {
    private final Logger logger = LogManager.getLogger(UrlCheckout.class);

    private Queue<UrlConf> urlsToCheck;
    private Queue<UrlResponse> checkedUrls;

    public UrlCheckout() {

    }

    public UrlCheckout(Queue<UrlConf> urlsToCheck, Queue<UrlResponse> checkedUrls) {
        this.urlsToCheck = urlsToCheck;
        this.checkedUrls = checkedUrls;
    }

    public Queue<UrlResponse> startUrlCheck() {
        while(urlsToCheck.iterator().hasNext()) {
            ResponseAnalyzer analyzer = new ResponseAnalyzer();

            checkedUrls.add(analyzer.analyze(urlsToCheck.poll()));
        }

        return checkedUrls;
    }

    @Override
    public void run() {
        this.startUrlCheck();
    }

    public Queue<UrlConf> getUrlsToCheck() {
        return urlsToCheck;
    }

    public void setUrlsToCheck(Queue<UrlConf> urlsToCheck) {
        this.urlsToCheck = urlsToCheck;
    }

    public Queue<UrlResponse> getCheckedUrls() {
        return checkedUrls;
    }

    public void setCheckedUrls(Queue<UrlResponse> checkedUrls) {
        this.checkedUrls = checkedUrls;
    }
}
