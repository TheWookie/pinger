package model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;

public class UrlPing {
    private TimeNotch timer = new TimeNotch();
    private HttpURLConnection connection;
    private String pageContent;
    private String urlName;

    public void ping(String urlToProcess) throws IOException {
        this.urlName = urlToProcess;

        URL url = new URL(urlToProcess);
        connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");

        timer.start();
        connection.connect();
        timer.stop();

        loadPageContent();
    }

    private String loadPageContent() throws IOException {
        BufferedReader r = new BufferedReader(new InputStreamReader(connection.getInputStream(),
                Charset.forName("UTF-8")));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = r.readLine()) != null) {
            sb.append(line);
        }

        pageContent = sb.toString();

        return pageContent;
    }

    public long getResponseTime() {
        return timer.getTime();
    }

    public int getResponseCode() throws IOException {
        return connection.getResponseCode();
    }

    public String getResponseMessage() throws IOException {
        return connection.getResponseMessage();
    }

    public String getPageContent() {
        return pageContent;
    }

    public String getUrlName() {
        return urlName;
    }

    public int getPageContentSize() {
        return pageContent.getBytes().length;
    }
}
