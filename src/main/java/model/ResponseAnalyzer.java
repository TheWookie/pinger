package model;

import entity.UrlConf;
import entity.UrlResponse;
import entity.UrlStatus;

import java.io.IOException;

/**
 * Class for analyzing response from pinged url.
 * Created by wookie on 6/20/17.
 */
class ResponseAnalyzer {
    private UrlPing pinger = new UrlPing();

    UrlResponse analyze(UrlConf urlConf) {
        try {
            pinger.ping(urlConf.getUrl());
            UrlResponse urlResponse = new UrlResponse();

            urlResponse.setStatusCode(pinger.getResponseCode());
            urlResponse.setResponseTime(pinger.getResponseTime());
            urlResponse.setConf(urlConf);
            urlResponse.setResponseSize(pinger.getPageContentSize());

            if(urlConf.getResponseStatus() != pinger.getResponseCode()) {
                urlResponse.setStatusName(UrlStatus.CRITICAL);
                return urlResponse;
            } else if(pinger.getPageContentSize() > urlConf.getResponseSize()) {
                urlResponse.setStatusName(UrlStatus.CRITICAL);
                return urlResponse;
            } else if(pinger.getResponseTime() <= urlConf.getResponseTimeOk()) {
                urlResponse.setStatusName(UrlStatus.OK);
            } else if(pinger.getResponseTime() <= urlConf.getResponseTimeWarning()) {
                urlResponse.setStatusName(UrlStatus.WARNING);
            } else {
                urlResponse.setStatusName(UrlStatus.CRITICAL);
            }
            if(urlConf.getResponseSubstr() != null) {
                if (!pinger.getPageContent().contains(urlConf.getResponseSubstr()))
                    urlResponse.setStatusName(UrlStatus.CRITICAL);
            }

            return urlResponse;
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }

    }

    void setPinger(UrlPing pinger) {
        this.pinger = pinger;
    }
}
