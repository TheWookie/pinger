package service;


import constant.GlobalConstants;
import datasource.file.FileDatasource;
import datasource.file.PropertiesParser;
import entity.UrlConf;
import entity.UrlResponse;
import model.UrlCheckout;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class UrlPingService {
    private static UrlPingService instance;

    private UrlPingService() {

    }

    public static UrlPingService getInstance() {
        if(instance == null) {
            instance = new UrlPingService();
        }

        return instance;
    }

    public Queue<UrlResponse> ping() {
        int threadsCount = loadThreadCount();

        Queue<UrlConf> urlsToCheck;
        try {
            urlsToCheck = new ConcurrentLinkedQueue<>(new FileDatasource().loadAll());
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
        Queue<UrlResponse> checkedUrls = new ConcurrentLinkedQueue<>();

        List<Thread> pool = new ArrayList<>(threadsCount);
        for(int i = 0; i < threadsCount; i++) {
            UrlCheckout checkout = new UrlCheckout(urlsToCheck, checkedUrls);

            checkout.start();

            pool.add(checkout);
        }

        try {
            for(Thread t : pool)
                t.join(); // may use join(long millis)

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return checkedUrls;
    }

    private int loadThreadCount() {
        return Integer.parseInt(new PropertiesParser()
                .loadFromMainConfig(GlobalConstants.THREAD_NUM_PROPERTY));
    }
}
