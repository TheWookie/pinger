package entity;

/**
 * Entity to save configuration of url which would be processed.
 * Created by wookie on 6/20/17.
 */
public class UrlConf {
    private String url;
    private int responseStatus;
    private long responseTimeOk;
    private long responseTimeWarning;
    private int responseSize;
    private String responseSubstr;

    public UrlConf() {

    }

    public UrlConf(String url, int responseStatus, long responseTimeOk, long responseTimeWarning, int responseSize) {
        this.url = url;
        this.responseStatus = responseStatus;
        this.responseTimeOk = responseTimeOk;
        this.responseTimeWarning = responseTimeWarning;
        this.responseSize = responseSize;
    }

    public UrlConf(String url, int responseStatus, long responseTimeOk, long responseTimeWarning, int responseSize, String responseSubstr) {
        this.url = url;
        this.responseStatus = responseStatus;
        this.responseTimeOk = responseTimeOk;
        this.responseTimeWarning = responseTimeWarning;
        this.responseSize = responseSize;
        this.responseSubstr = responseSubstr;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(int responseStatus) {
        this.responseStatus = responseStatus;
    }

    public long getResponseTimeOk() {
        return responseTimeOk;
    }

    public void setResponseTimeOk(long responseTimeOk) {
        this.responseTimeOk = responseTimeOk;
    }

    public long getResponseTimeWarning() {
        return responseTimeWarning;
    }

    public void setResponseTimeWarning(long responseTimeWarning) {
        this.responseTimeWarning = responseTimeWarning;
    }

    public int getResponseSize() {
        return responseSize;
    }

    public void setResponseSize(int responseSize) {
        this.responseSize = responseSize;
    }

    public String getResponseSubstr() {
        return responseSubstr;
    }

    public void setResponseSubstr(String responseSubstr) {
        this.responseSubstr = responseSubstr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UrlConf urlConf = (UrlConf) o;

        if (responseStatus != urlConf.responseStatus) return false;
        if (responseTimeOk != urlConf.responseTimeOk) return false;
        if (responseTimeWarning != urlConf.responseTimeWarning) return false;
        if (responseSize != urlConf.responseSize) return false;
        if (!url.equals(urlConf.url)) return false;
        return responseSubstr != null ? responseSubstr.equals(urlConf.responseSubstr) : urlConf.responseSubstr == null;
    }

    @Override
    public int hashCode() {
        int result = url.hashCode();
        result = 31 * result + responseStatus;
        result = 31 * result + (int) (responseTimeOk ^ (responseTimeOk >>> 32));
        result = 31 * result + (int) (responseTimeWarning ^ (responseTimeWarning >>> 32));
        result = 31 * result + responseSize;
        result = 31 * result + (responseSubstr != null ? responseSubstr.hashCode() : 0);
        return result;
    }
}
