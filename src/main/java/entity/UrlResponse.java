package entity;

public class UrlResponse {
    private UrlConf conf;
    private int statusCode;
    private UrlStatus statusName;
    private long responseTime;
    private int responseSize;

    public UrlResponse() {
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public UrlStatus getStatusName() {
        return statusName;
    }

    public void setStatusName(UrlStatus statusName) {
        this.statusName = statusName;
    }

    public long getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(long responseTime) {
        this.responseTime = responseTime;
    }

    public int getResponseSize() {
        return responseSize;
    }

    public void setResponseSize(int responseSize) {
        this.responseSize = responseSize;
    }

    public UrlConf getConf() {
        return conf;
    }

    public void setConf(UrlConf conf) {
        this.conf = conf;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UrlResponse that = (UrlResponse) o;

        if (statusCode != that.statusCode) return false;
        if (responseTime != that.responseTime) return false;
        if (responseSize != that.responseSize) return false;
        if (!conf.getUrl().equals(that.getConf().getUrl())) return false;
        return statusName == that.statusName;
    }

    @Override
    public int hashCode() {
        int result = conf.getUrl().hashCode();
        result = 31 * result + statusCode;
        result = 31 * result + statusName.hashCode();
        result = 31 * result + (int) (responseTime ^ (responseTime >>> 32));
        result = 31 * result + responseSize;
        return result;
    }
}
