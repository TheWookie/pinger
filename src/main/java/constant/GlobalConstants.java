package constant;

/**
 * Created by wookie on 6/21/17.
 */
public interface GlobalConstants {
    String GLOBAL_CONF_PATH = "/conf.properties";
    String THREAD_NUM_PROPERTY = "thread.num";
}
