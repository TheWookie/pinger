package datasource.file;

import constant.GlobalConstants;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesParser {
    private Properties props;

    public void load(InputStream is) throws IOException {
        props = new Properties();
        props.load(is);
    }

    public void load(String path) throws IOException {
        try(InputStream is = getClass().getResourceAsStream(path)) {
            load(is);
        }
    }

    public String getProperty(String propName) {
        return props.getProperty(propName);
    }

    public String loadFromMainConfig(String propName) {
        try {
            this.load(GlobalConstants.GLOBAL_CONF_PATH);

            return this.getProperty(propName);
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }
}
