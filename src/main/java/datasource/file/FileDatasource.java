package datasource.file;


import constant.GlobalConstants;
import datasource.Datasource;
import entity.UrlConf;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileDatasource implements Datasource {
    private final Logger logger = LogManager.getLogger(FileDatasource.class);

    public static final String URL_PROPERTY = "url";
    public static final String STATUS_PROPERTY = "response.status";
    public static final String TIME_OK_PROPERTY = "response.time.ok";
    public static final String TIME_WARNING_PROPERTY = "response.time.warning";
    public static final String SIZE_PROPERTY = "response.size";
    public static final String SUBSTR_PROPERTY = "response.substr";
    public static final String URL_CONFIG_DIR_PROPERTY = "url.config.dir";

    private List<UrlConf> entities = new ArrayList<>();
    private PropertiesParser props = new PropertiesParser();

    @Override
    public List<UrlConf> loadAll() throws IOException {

        for(File f : getAllProperties()) {
            try (InputStream is = new FileInputStream(f)) {
                props.load(is);

                entities.add(extract());
            } catch (IOException e) {
                logger.info("Wrong property file in path was ignored: " + f.getName());
            }
        }

        return entities;
    }

    private UrlConf extract() throws IOException {
        UrlConf url = new UrlConf();

        try {
            url.setUrl(props.getProperty(URL_PROPERTY));
            url.setResponseStatus(Integer.parseInt(props.getProperty(STATUS_PROPERTY)));
            url.setResponseSize(Integer.parseInt(props.getProperty(SIZE_PROPERTY)));
            url.setResponseSubstr(props.getProperty(SUBSTR_PROPERTY));
            url.setResponseTimeOk(Long.parseLong(props.getProperty(TIME_OK_PROPERTY)));
            url.setResponseTimeWarning(Long.parseLong(props.getProperty(TIME_WARNING_PROPERTY)));
        } catch (Exception e) {
            throw new IOException(e.getMessage());
        }

        return url;
    }

    private List<File> getAllProperties() throws IOException {

        props.load(GlobalConstants.GLOBAL_CONF_PATH);
        String path = props.getProperty(URL_CONFIG_DIR_PROPERTY);

        try (Stream<Path> paths = Files.walk(Paths.get(path))) {
            return paths
                    .filter(Files::isRegularFile)
                    .map(Path::toFile)
                    .collect(Collectors.toList());
        }
    }

}
