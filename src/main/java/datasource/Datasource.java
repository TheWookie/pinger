package datasource;

import entity.UrlConf;

import java.io.IOException;
import java.util.List;


public interface Datasource {
    List<UrlConf> loadAll() throws IOException;
}
